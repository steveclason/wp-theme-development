'use strict';

const THEME_DIRECTORY = 'src';

var gulp = require('gulp');
var sass = require( 'gulp-sass' );
var sourcemaps = require('gulp-sourcemaps');
var postcss = require( 'gulp-postcss' );
var rename = require( 'gulp-rename' );
var minify = require( 'gulp-minify-css' );
var autoprefixer = require( 'gulp-autoprefixer' );
var util = require( 'gulp-util' );
var log = util.log;
//require( 'stylelint' )(),

gulp.task('default', function() {
  // place code for your default task here
});

// Run the normalize task once only.
gulp.task('normalize', function () {
  return gulp.src('./' + THEME_DIRECTORY + '/sass/style.scss').pipe(
    postcss([
      require('postcss-normalize')({ /* options */ })
    ])
    ).pipe(
        gulp.dest('./' + THEME_DIRECTORY + '/sass')
    );
});

gulp.task('sass', function () {
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./' + THEME_DIRECTORY + '/sass/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe( autoprefixer())
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './' + THEME_DIRECTORY ))

    .pipe( rename({ suffix: '.min' }))
    .pipe(minify())
    .pipe(gulp.dest( './' + THEME_DIRECTORY ));
});

// gulp.task('sass:watch', function () {
// 	gulp.watch('./inn97win/sass/**/*.scss', ['sass']);
// });
