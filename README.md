# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Overhead and utilities for WordPress theme development. Assumes use of SASSified \_s, http://underscores.me/ or https://github.com/automattic/_s, this project sets up build tools.
Initially this only handles CSS by autoprefixing using browserlist, then compliling and minifying .scss files and creating a style map.

Includes stylelint file.

* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Set Up
Insert a SASSified version of \_s into the new theme project, then copy the contents from here, then set up the environment in the theme folder using node.

* Configuration
* Dependencies
  - gulp
  - gulp-postcss
  - gulp-sass
  - gulp-autoprefixer
  - gulp-sourcemaps
  - gulp-util
  - postcss-normalize
* Database configuration
* How to run tests
* Deployment instructions
* TODOs
  - include a FTP utility

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Steve Clason, steve@steveclason.com
* Other community or team contact
